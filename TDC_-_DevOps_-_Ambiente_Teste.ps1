﻿
# create a DSC configuration to install IIS FINO - AMBIENTE DE TESTE DEVOPS TDC RECIFE 2019
configuration IISConfig {

    # define input parameter
    param(
        [string[]]$computername = 'localhost'
    )

    # target machine(s) based on input param
    node $computername {

        # configure the LCM
        LocalConfigurationManager {
            ConfigurationMode = "ApplyAndAutoCorrect"
            ConfigurationModeFrequencyMins = 15
            RefreshMode = "Push"
        }

        # install the IIS server role
        WindowsFeature IIS {
            Ensure = "Present"
            Name = "web-Server"
        }

        # install the IIS remote management service
        WindowsFeature IISManagement {
            Name = 'Web-Mgmt-Service'
            Ensure = 'Present'
            Dependson = @('[WindowsFeature]IIS')
        }

        # install the IIS management console
        WindowsFeature IISManagementConsole {
            Name = 'Web-Mgmt-Console'
            Ensure = 'Present'
            Dependson = @('[WindowsFeature]IISManagement')
        }

                  # install the IIS Basic
                        WindowsFeature BasicAuthentication  {
                        Name = 'Web-Basic-Auth'
                        Ensure = 'Present'
                        Dependson = @('[WindowsFeature]IISManagement')
        }

                  # install the IIS Windows
                        WindowsFeature WindowsAuthentication   {
                        Name = 'Web-Windows-Auth'
                        Ensure = 'Present'
                        Dependson = @('[WindowsFeature]BasicAuthentication')
        }


    }

}

#create the configuration (.mof) - AMBIENTE DE TESTE SRVADMAO01
IISConfig -Computername srvadmao01.mao.arqengti.local -OutputPath "C:\TDC-DevOps Recife 2019\Ambiente de Fabrica\Script-PDSC\Teste"

#push the configuration to SRVADMAO01 - AMBIENTE DE TESTE
Start-DscConfiguration –Path "C:\TDC-DevOps Recife 2019\Ambiente de Fabrica\Script-PDSC\Teste" –Wait –Verbose -Force

#Criacao dos Registros DNS - Domain e HostA
Add-DnsServerResourceRecordA -Name "www.devopstestetdcrecife2019" -ZoneName "arqengti.local" -AllowUpdateAny -IPv4Address "192.168.0.4"

#Copia dos Arquivos - Para os Servidores Teste - IMPORTANTE PASTA COMPARTILHADA SITES
xcopy /e "\\SRVAD2012SD01\TDC-DevOps Recife 2019" "\\SRVADMAO01\Sites" /y

#enter powershell remote session - DEPLOY AMBIENTE TESTE
Enter-PSSession -ComputerName SRVADMAO01

#Import Module 
Import-Module WebAdministration 

#Create WebAppPool TDC-RECIFE2019 (Teste)
$iisAppPoolName1 = "Ambiente Fabrica Teste TDC-RECIFE2019"
$appPool1 = New-WebAppPool -Name $iisAppPoolName1  
$appPool1.managedPipelineMode = "Classic"
$appPool1 |Set-Item
$identity = @{  `
    identitytype="SpecificUser"; `
    username="fabio.fol@arqengti.local"; `
    password="P@ssw0rd" `
}
Set-ItemProperty -Path "IIS:\AppPools\Ambiente Fabrica Teste TDC-RECIFE2019" -name "processModel" -value $identity

#Criacao dos WebSites no IIS - Virtual Host - Host HeaderTDC-RECIFE2019 (Teste)
$site = $site = new-WebSite -name "Ambiente Fabrica Teste TDC-RECIFE2019" -PhysicalPath "C:\Sites\Ambiente de Fabrica\Sites\Teste" -IpAddress "192.168.0.4" -HostHeader "www.devopstestetdcrecife2019.arqengti.local" -ApplicationPool "Ambiente Fabrica Teste TDC-RECIFE2019" -force
$iisSitePath = "IIS:\Sites\Ambiente Fabrica Teste TDC-RECIFE2019"
$website = get-item $iisSitePath
$website.virtualDirectoryDefaults.userName = "arqengti.local\fabio.fol"
$website.virtualDirectoryDefaults.password = "P@ssw0rd"
$website | set-item

#Administracao IIS Start
IIS:
dir sites
dir AppPools

Start-Website "Ambiente Fabrica Teste TDC-RECIFE2019"

Start-WebAppPool "Ambiente Fabrica Teste TDC-RECIFE2019"

#Flush Dns
ipconfig /flushdns